.. Travail sur codebug documentation master file, created by
   sphinx-quickstart on Tue Dec 15 14:03:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Travail sur codebug's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


..
	Indices and tables
	==================
..
	* :ref:`genindex`
	* :ref:`modindex`
	* :ref:`search`

Exercices
=========

Ces exercices permettent de faire découvrir la programmation embarqué.
La programmation embarqué est la programmation sur carte éléctronique.
La programmation de systèmes complexes est réalisé par l'assemblage et
l'utilisation de programme simple. Le programme d'un satellite est complexe. 
C'est pourquoi pour programmer un satellite, on réalise une multitude de petits programmes
qui une fois assemblés, réalisent des fonctionnalités complexes.


Lorsque vous changez d'exercice, n'effacez pas votre programme. Ouvrez un nouvel onglet.

Une LED est un petit composant éléctronique qui émet de la lumière.
Sur le codebug il y en 25 de couleur rouge. Elles forment une "matrice de LEDs".

1 - allumer une LED
-------------------
	- Allumer une simple LED.
	
2 - allumer une LED 2
---------------------
	- Allumer une LED en appuyant sur le bouton A ou le bouton B.
	
3 - allumer une LED 3
---------------------
	- Allumer une LED en appuyant sur le bouton A et éteindre la LED en appuyant sur le bouton B.
	
4 - chenillard 1
----------------
	- Réaliser un chenillard.
	
Un chenillard est une ligne de LED qui se déplace sur une matrice de LEDs à la manière du jeu "snake".
Dans cet exercice le but est de faire tourner 5 LEDs alignées, autour de la matrice de LED du codebug.

5 - chenillard 2
----------------

	- Réaliser un chenillard et le stoper lorsqu'on appuie sur le bouton A.
	- Relancer le chenillard en appuyant sur le bouton B.
	
6 - chenillard 3
----------------

	- Changer la direction du chenillard lors de l'appuie sur le bouton A.
	
7 - compteur 1
--------------

	- coder les chiffres de 0 à 9 sur la matrice de LEDs
	- Réaliser un compteur de secondes allant de 0 à 9.
	- Réinitialiser le compteur automatiquement lorsque 9 est atteint.

Conseil: utilisez les variables pour enregistrer le motif des chiffres.
	
8 - compteur 2
--------------

	- Remettre le compteur à 0 lors de l'appuie sur le bouton A.
	
9 - compteur 3
--------------

	- Faire défiler le compteur de 9 à 0.
	
10 - chenillard 5
-----------------

	- Réaliser un chenillard qui se déplace 2 fois plus rapidement à chaque appuie sur le bouton A.
	
11 - chenillard 6
-----------------

	- Réaliser un chenillard qui s'arrête sur une LED spécifique.
	- Faire redémarrer le chenillard en appuyant sur le bouton A.
	









